FROM alpine:3.12 as builder

RUN apk add --no-cache git go build-base
RUN git clone https://git.sr.ht/~fnux/matrix-alertmanager-receiver \
 && cd matrix-alertmanager-receiver \
 && go build -v

FROM alpine:3.12
COPY --from=builder /matrix-alertmanager-receiver/matrix-alertmanager-receiver /usr/local/bin/matrix-alertmanager-receiver
VOLUME /data
CMD "/usr/local/bin/matrix-alertmanager-receiver -config /data/config.toml"
